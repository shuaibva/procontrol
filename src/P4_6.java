import jdk.nashorn.internal.ir.WhileNode;

/**
 * Created by shuaibahmed on 4/5/15.
 */
import java.util.Scanner;

public class P4_6
{
    public static void main(String[] args)
    {
        boolean first = true;
        System.out.println("Enter values seperated by space (-1 to exit): ");
        Scanner in = new Scanner(System.in);
        int value = in.nextInt();
        int minimum = 0;
        while (value != -1)
        {
            if (first)
            {
                minimum = value;
                first = false;
            }
            else if (value < minimum)
            {
                minimum = value;
            }
            value = in.nextInt();
        }
        System.out.println("Minimum is " + minimum);

    }
}
