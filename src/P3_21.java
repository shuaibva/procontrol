/**
 * Created by shuaibahmed on 4/5/15.
 * Program receives income and prints correct income tax
 */
import java.util.Scanner;

public class P3_21
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter your income: ");
        int income = in.nextInt();
        double tax = 00;
        if (income <= 50000)
        {
            tax = income * .01;
        }
        else if (income > 50000 && income <= 75000)
        {
            tax = 50000*.01 + (income - 50000)* .02;
        }
        else if (income > 75000 && income <= 100000)
        {
            tax = 50000*.01 + 25000*.02 + (income - 75000)* .03;
        }
        else if (income > 100000 && income <= 250000)
        {
            tax = 50000*.01 + 25000*.02 + 25000*.03 + (income - 100000)*.04;
        }
        else if (income > 250000 && income <= 500000)
        {
            tax = 50000*.01 + 25000*.02 + 25000*.03 + 150000*.04 +
                    (income - 250000)*.05;
        }
        else if (income > 500000)
        {
            tax = 50000*.01 + 25000*.02 + 25000*.03 + 150000*.04 +
                    250000*.05 + (income - 500000)*.06;
        }

        System.out.println("Your tax liability is: " + tax);
    }
}
