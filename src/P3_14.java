import java.util.Scanner;

public class P3_14 {
    public static void main(String[] args) {
        //** Method 1:
        //input - two digit shorthand
        //outpout - long form (kind of suit)

        // Store input
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the card notation: ");
        String cardNotation = in.next();
        String kind = cardNotation.substring(0, 1);
        String suit = cardNotation.substring(1, 2);

        // Logic for kind

        if (kind.equals("A"))
            {
            kind = "Ace";
            }
        else if (kind.equals("K"))
            {
            kind = "King";
            }
        else if (kind.equals("Q"))
            {
            kind = "Queen";
            }
        else if (kind.equals("J"))
            {
            kind = "Jack";
            }
        else if (kind.equals("1") || kind.equals("2") || kind.equals("3")
                || kind.equals("4") || kind.equals("5") || kind.equals("6")
                || kind.equals("7") || kind.equals("8") || kind.equals("9")
                || kind.equals("10"))
            {
            ;
            }
        else
            {
                System.out.println("Entry incorrect, result invalid, " +
                        "run program again");
            }

        //Logic for suit
        if (suit.equals("C"))
        {
            suit = "Clubs";
        }
        else if (suit.equals("D"))
        {
            suit = "Diamonds";
        }
        else if (suit.equals("H"))
        {
            suit = "Hearts";
        }
        else if (suit.equals("S"))
        {
            suit = "Spades";
        }
        else {
            System.out.println("Entry incorrect, result invalid, " +
                    "run program again");
        }

        // Print description

        System.out.println(kind + " of " + suit);
    }
}

//couldn't figure out how to handle exceptions correctly