import java.util.Scanner;

public class P2_17 {
    public static void main(String[] args) {
        //** Method 1:
        // input - 2 military time strings from user
        // output - difference formatted with hours and minutes
        //Algorithm:
        //      lop off the first two digits from each input, subtract
        //      print hours
        //      lop off the final two digits from each, subtract
        //      print minutes


        Scanner in = new Scanner(System.in);
        System.out.print("Please enter the first time: ");
        String timeOne = in.next();
        System.out.print("Please enter the second time: ");
        String timeTwo = in.next();

        //grab hours, convert to int
        String strHourOne = timeOne.substring(0,2);
        String strHourTwo = timeTwo.substring(0,2);
        int hourOne = Integer.parseInt(strHourOne);
        int hourTwo = Integer.parseInt(strHourTwo);
        //grab minutes, convert to int
        String strMinOne = timeOne.substring(2,4);
        String strMinTwo = timeTwo.substring(2,4);
        int minOne = Integer.parseInt(strMinOne);
        int minTwo = Integer.parseInt(strMinTwo);

        //print hours and minutes
        System.out.print(Math.abs(hourTwo - hourOne) + " hours ");
        System.out.print(Math.abs(minTwo - minOne) + " minutes");

    }
}