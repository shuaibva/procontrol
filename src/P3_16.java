/**
 * Created by shuaibahmed on 4/5/15.
 * Program: input 3 strings, output alphabetically sorted strings
 */
import java.util.Scanner;

public class P3_16 {
    public static void main(String[] args)
    {
        //input 3 strings

        Scanner in = new Scanner(System.in);
        System.out.print("Enter three strings: ");
        String strA = in.next();
        String strB = in.next();
        String strC = in.next();
        System.out.println("");

        //sort 3 strings
        if (strA.compareTo(strB) < 0 && strA.compareTo(strC) < 0)
        {
            System.out.println(strA);
            if (strB.compareTo(strC) < 0)
            {
                System.out.println(strB);
                System.out.println(strC);
            }
            else
            {
                System.out.println(strC);
                System.out.println(strB);
            }
        }
        else if (strB.compareTo(strA) < 0 && strB.compareTo(strC) < 0)
        {
            System.out.println(strB);
            if (strA.compareTo(strC) < 0)
            {
                System.out.println(strA);
                System.out.println(strC);
            }
            else
            {
                System.out.println(strC);
                System.out.println(strA);
            }
        }
        else if (strC.compareTo(strA) < 0 && strC.compareTo(strB) < 0)
        {
            System.out.println(strC);
            if (strA.compareTo(strB) < 0)
            {
                System.out.println(strA);
                System.out.println(strB);
            }
            else
            {
                System.out.println(strB);
                System.out.println(strA);
            }
        }
        // what if inputs A,B are identical
        else if (strA.equals(strB) && strA.equals(strC))
        {
            System.out.println(strA);
            System.out.println(strA);
            System.out.println(strA);
        }
        else if (strA.equals(strB) && strA.compareTo(strC) < 0)
        {
            System.out.println(strA);
            System.out.println(strB);
            System.out.println(strC);
        }
        else if (strA.equals(strB) && strA.compareTo(strC) > 0)
        {
            System.out.println(strC);
            System.out.println(strA);
            System.out.println(strB);
        }
        // what if inputs A,C are identical
        else if (strA.equals(strC) && strA.compareTo(strB) < 0)
        {
            System.out.println(strA);
            System.out.println(strC);
            System.out.println(strB);
        }
        else if (strA.equals(strC) && strA.compareTo(strB) > 0)
        {
            System.out.println(strB);
            System.out.println(strA);
            System.out.println(strC);
        }
        // what if inputs B,C are identical
        else if (strB.equals(strC) && strB.compareTo(strA) < 0)
        {
            System.out.println(strB);
            System.out.println(strC);
            System.out.println(strA);
        }
        else if (strB.equals(strC) && strB.compareTo(strA) > 0)
        {
            System.out.println(strA);
            System.out.println(strB);
            System.out.println(strC);
        }

    }
}

// There's a better way to do this I think