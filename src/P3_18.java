/**
 * Created by shuaibahmed on 4/5/15.
 * program accepts numerical values for month and day and returns one of
 * the four seasons
 */
import java.util.Scanner;

public class P3_18 {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Give a month and day: ");
        int month = in.nextInt();
        int day = in.nextInt();
        String season = null;
        if (month <= 12 && day <= 31)
        {
            if (month >= 1 && month <= 3)
            {
                season = "Winter";
            }
            else if (month >= 4 && month <= 6)
            {
                season = "Spring";
            }
            else if (month >= 7 && month <= 9)
            {
                season = "Summer";
            }
            else if (month >= 10 && month <= 12)
            {
                season = "Fall";
            }
            if (month % 3 == 0 && day >= 21)
            {
                if (season == "Winter")
                {
                    season = "Spring";
                }
                else if (season == "Spring")
                {
                    season = "Summer";
                }
                else if (season == "Summer")
                {
                    season = "Fall";
                }
                else
                {
                    season = "Winter";
                }
            }
            System.out.println(season);
        }
        else
        {
            System.out.println("Incorrect date range entered");
        }

    }
}

// doesn't capture every exception (someone could still enter an invalid day
// like February 31st