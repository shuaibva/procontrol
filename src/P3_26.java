/**
 * Program converts integer input, less than 3999 in returns roman numeral
 */
import java.util.Scanner;

public class P3_26
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("What number would you like to convert?: ");
        int userInput = in.nextInt();
        //Strategy - break apart into ones, tens, hundreds. Analyze seperately
        //and recompose
        if (userInput <= 3999 && userInput >=0)
        {
            //Decompose input into place values
            int intThousands = userInput / 1000;
            int intHundreds = (userInput % 1000) / 100;
            int intTens = ((userInput % 1000) % 100) / 10;
            int intOnes = ((userInput % 1000) % 100) % 10;

            //ones place logic
            String romOnes = "";

            if (intOnes == 1) {
                romOnes = "I";
            } else if (intOnes == 2) {
                romOnes = "II";
            } else if (intOnes == 3) {
                romOnes = "III";
            } else if (intOnes == 4) {
                romOnes = "IV";
            } else if (intOnes == 5) {
                romOnes = "V";
            } else if (intOnes == 6) {
                romOnes = "VI";
            } else if (intOnes == 7) {
                romOnes = "VII";
            } else if (intOnes == 8) {
                romOnes = "VIII";
            } else if (intOnes == 9) {
                romOnes = "IX";
            }

            // tens place logic
            String romTens = "";

            if (intTens == 1) {
                romTens = "X";
            } else if (intTens == 2) {
                romTens = "XX";
            } else if (intTens == 3) {
                romTens = "XXX";
            } else if (intTens == 4) {
                romTens = "XL";
            } else if (intTens == 5) {
                romTens = "L";
            } else if (intTens == 6) {
                romTens = "LX";
            } else if (intTens == 7) {
                romTens = "LXX";
            } else if (intTens == 8) {
                romTens = "LXXX";
            } else if (intTens == 9) {
                romTens = "XC";
            }

            // hundreds place logic
            String romHundreds = "";

            if (intHundreds == 1) {
                romHundreds = "C";
            } else if (intHundreds == 2) {
                romHundreds = "CC";
            } else if (intHundreds == 3) {
                romHundreds = "CCC";
            } else if (intHundreds == 4) {
                romHundreds = "CD";
            } else if (intHundreds == 5) {
                romHundreds = "D";
            } else if (intHundreds == 6) {
                romHundreds = "DC";
            } else if (intHundreds == 7) {
                romHundreds = "DCC";
            } else if (intHundreds == 8) {
                romHundreds = "DCCC";
            } else if (intHundreds == 9) {
                romHundreds = "CM";
            }

            // thousands place logic
            String romThousands = "";

            if (intThousands == 1) {
                romThousands = "M";
            } else if (intThousands == 2) {
                romThousands = "MM";
            } else if (intThousands == 3) {
                romThousands = "MMM";
            }
            System.out.print(romThousands + romHundreds + romTens + romOnes);
        }
        else
        {
            System.out.println("Number out of range!");
        }
    }
}

//could this code be more readeable? and less error prone?