/**
 * Created by shuaibahmed on 4/5/15.
 * program prints primes up to given number
 */
import java.util.Scanner;

public class P4_18
{
    public static void main(String[] args)
    {
        System.out.println("Enter a number: ");
        Scanner in = new Scanner(System.in);
        int ceiling = in.nextInt();
        if (ceiling >= 2)
        {
            int currentNum = 2;
            while (currentNum <= ceiling)
            {
                int j = 2;
                int result = 0;
                while (j <= currentNum/2)
                {
                    if (currentNum % j == 0)
                    {
                        result = 1;
                    }
                    j++;
                }
                if (result != 1)
                {
                    System.out.println(currentNum);
                }
                currentNum++;
            }
        }
        else
        {
            System.out.println("Number out of range");
        }
    }
}
